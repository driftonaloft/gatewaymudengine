//player.h


#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <memory>

#include "actor.h"

class Player : public Actor
{
public:
    Player();
    ~Player();

    int commandFlags;

    std::string File;

    void Save(std::string File);
    void Load(std::string File);

    void Save(pugi::xml_node);
    void Load(pugi::xml_node);
};


class PlayerProcessor
{
public:
    PlayerProcessor();
    ~PlayerProcessor();

    bool Init();
    bool Frame();
    bool Shutdown();

    std::list<std::shared_ptr<Player>> playerList;
};
#endif
