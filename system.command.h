//system.command.h

#include <memory>
#include <vector>
#include <string>

#include "actor.h"

#ifndef SYSTEM_COMMAND_H
#define SYSTEM_COMMAND_H

int cmd_shutdown(std::shared_ptr<Actor> A, std::vector<std::string> tokens);
int cmd_status(std::shared_ptr<Actor> A, std::vector<std::string> tokens);


#endif
