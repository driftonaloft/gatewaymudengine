//command.cpp

#include "command.h"
//#include "stringutilites.h"


Command::Command()
{
}

Command::~Command()
{
}

CommandParser::CommandParser()
{
}

CommandParser::~CommandParser()
{
}

bool isWhiteSpace(char C)
{
	switch(C)
	{
		case '\t':
		case '\0':
		case '\v':
		case '\n':
		case '\r':
		case 27:
		case 32:
			return true;
			break;
		default:
			return false;
			break;
	}
}

void CommandParser::RegisterCommand(std::string CommandName, std::function <int(std::shared_ptr<Actor>, std::vector<std::string>)> fp, std::string Usage)
{
	auto cmd = std::make_shared<Command>();
	
	cmd->cmdName = CommandName;
	cmd->fp = fp;
	cmd->usage = Usage;
	cmd->isExact = false;
	
	Commands.push_back(cmd);
}

int CommandParser::Parse(std::shared_ptr<Actor> actor, std::string Input)
{
	std::vector<std::string> Tokens = Tokenize(Input);
	
    if(Tokens.size()>0)
    {
        for (auto C: Commands)
        {
            if(C->cmdName.find(Tokens[0]) == 0)
            {
                if(C->isExact)
                {
                    if(Tokens[0] == C->cmdName)
                    {
                        C->fp(actor,Tokens);
                        return 1;
                    }
                }
                else
                {
                    C->fp(actor,Tokens);
                    return 1;
                }
            }
        }
    }
	return 0;
}

std::vector<std::string> CommandParser::Tokenize(std::string Input)
{
    std::vector<std::string> Tokens;
	
	enum TokenState
	{
		WhiteSpace,
		SingleQuote,
		DoubleQuote,
		Text
	};
	
	TokenState TS=WhiteSpace;
	std::string Token;
	
	auto PushToken = [&](){
		Tokens.push_back(Token);
		Token.clear();
	};
	
	for (auto C = Input.begin(); C != Input.end(); C++)
	{
		switch (TS)
		{
			case WhiteSpace:
			{
				if(*C == '\'') {
					TS = SingleQuote;
				} else if (*C == '\"') {
					TS = DoubleQuote;
				} else if (!isWhiteSpace(*C)) {
					TS = Text;
                    Token += *C;
				}
			}break;
			case SingleQuote:
			{
				if(*C == '\'') {
					PushToken();
                    TS = WhiteSpace;
				} else {
					Token += *C;
				}
			}break;
			case DoubleQuote:
			{
				if(*C == '\"') {
					PushToken();
                    TS = WhiteSpace;
				} else {
					Token += *C;
				}
			}break;
			case Text:
			{
				if(*C == '\'') {
					PushToken();
                    TS = SingleQuote;
				} else if( *C == '\"') {
					PushToken();
                    TS = DoubleQuote;
				} else if( isWhiteSpace(*C)) {
					PushToken();
                    TS = WhiteSpace;
				} else {
					Token += *C;
				}
			}break;
		}
	}
    
    PushToken();
	
    return Tokens;
}
