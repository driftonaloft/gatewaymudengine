//mud.h
//

#ifndef MUD_H
#define MUD_H

#include <string>
#include <memory>
#include <thread>
#include <chrono>

#include "network.h"
#include "accounts.h"
#include "player.h"
#include "command.h"

class Server;

class Mud
{
public:
    Mud();
    virtual ~Mud();

    int startup(std::string configFile);
    int run();
    int shutdown();

    void halt();
    
    std::string version;

	std::shared_ptr<AccountDB> accountDB;
	std::shared_ptr<Server> server;
    std::shared_ptr<PlayerProcessor> playerProc;
    std::shared_ptr<CommandParser> cmdParser;
private:
    bool running;
};

#endif//MUD_H
