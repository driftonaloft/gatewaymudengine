//mud.cpp

#include <iostream>


#include "mud.h"
#include "network.h"

Mud::Mud()
{
}

Mud::~Mud()
{
}

int Mud::startup(std::string configFile)
{
    version = "0.0.20";
    
    running = true;
    if(!server->Init(5891,64))
    {
    	std::cout << "error server" << std::endl;
    	return 0;
	}

	if(!accountDB->init("accounts.xml"))
	{
		std::cout << "error accountDB" << std::endl;
		return 0;
	}

    if(!playerProc->Init())
    {
        std::cout << "[PlayerProc] Error" << std::endl;
    }

	cmdParser = std::make_shared<CommandParser> ();
	cmdParser->RegisterCommand("@shutdown",cmd_shutdown,"@shutdown <message>");
    cmdParser->RegisterCommand("@status",cmd_status,"@status");
    cmdParser->RegisterCommand("@disconnect",cmd_exit,"@exit");
    cmdParser->RegisterCommand("who",cmd_who,"who");
    cmdParser->RegisterCommand("tell",cmd_tell,"tell <target> <message>");
    //cmdParser->RegisterCommand("chat",cmd_chat,"chat <message>");
    
    

    return 1;
}

using namespace std::literals::chrono_literals;

int Mud::run()
{
    std::cout <<  "Running" << std::endl;
    while (running)
    {
		auto start = std::chrono::high_resolution_clock::now();

	//	accountDB->frame();
    	server->Frame();
        playerProc->Frame();
        
        auto end = std::chrono::high_resolution_clock::now();
        auto sleepdur = 50ms - std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

        if(sleepdur > 0ms)
        {
            std::this_thread::sleep_for(sleepdur);
        }
        else
        {
            std::cout<< "sleep time elasped" << std::endl;
        }
    }
    return 1;
}

int Mud::shutdown()
{
    std::cout << "shutting down server" << std::endl;
    
	server->Shutdown();
	accountDB->saveDB("accounts.xml");
    playerProc->Shutdown();

    return 1;
}

void Mud::halt()
{
    running = false;
}

