//actor.h
#include <memory>

#include "mudobject.h"

#ifndef ACTOR_H
#define ACTOR_H

class Client;

class Actor : public MudObject
{
public:
    Actor();
    ~Actor();

    std::shared_ptr<Client> client;
    int state;
    

    //MudLocation Location;
};

#endif
