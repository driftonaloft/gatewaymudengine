//accounts.cpp
//


#include "pugixml.hpp"

#include "accounts.h"
#include "network.h"
#include "mud.h"

#include <iostream>

Account::Account()
{
    resetPassword = false;
    newAccount = false;
}

Account::~Account()
{

}

//AccountDB
AccountDB::AccountDB()
{
}

AccountDB::~AccountDB()
{
}

bool AccountDB::init(std::string AccountDBFile)
{
    if(!loadDB(AccountDBFile)){
        std::cout<<"[AccountDB] -- NO ACCOUNTS First Logging to become ADMIN" << std::endl;
    }
    return true;
}

std::shared_ptr<Account> AccountDB::getAccount(std::string accountName)
{
    for(auto account : accountList)
    {
        //if (tolower(account->name)== tolower(accountName))
        if(account->name == accountName)
        {
            return account;
        }
    }

    return std::shared_ptr<Account>();
}

bool AccountDB::loadDB(std::string AccountDBFile)
{
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(AccountDBFile.c_str());

    if(result)
    {
    	std::cout << "[AccountDB] - Loading Accounts " ;
        pugi::xml_node rootNode;

        rootNode = doc.child("AccountDB");
        for(auto childNode : rootNode.children("account")){
			auto newAccount = std::make_shared<Account>();

			newAccount->name = childNode.child_value ("name");
			newAccount->password = childNode.child_value("password");
			newAccount->file = childNode.child_value("file");
			newAccount->isSaveable = true;

			accountList.push_back(newAccount);
			std::cout  << "+";
        }
        std::cout << " finished" << std::endl;
        return true;
    }
    else {
        std::cout << "[AccountDB] failed to loadXML " << AccountDBFile << " file not found" << std::endl;
        return false;
    }
}

bool AccountDB::saveDB(std::string File)
{
	pugi::xml_document doc;
	pugi::xml_node root = doc.append_child("AccountDB");
	pugi::xml_node child;

	for( auto account : accountList)
	{
		if(account->isSaveable){
			child = root.append_child("account") ;
			child.append_child("name").append_child(pugi::node_pcdata).set_value(account->name.c_str());
			child.append_child("file").append_child(pugi::node_pcdata).set_value(account->file.c_str());
			child.append_child("password").append_child(pugi::node_pcdata).set_value(account->password.c_str());
		}
	}
	return doc.save_file(File.c_str());
}
