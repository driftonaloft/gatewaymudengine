//
//  player.command.cpp
//  Mud11
//
//  Created by drifton on 7/5/15.
//  Copyright (c) 2015 FracturedWire. All rights reserved.
//

#include <memory>
#include <string>
#include <vector>

#include "player.command.h"

extern std::shared_ptr<Mud> mud;

int cmd_exit(std::shared_ptr<Actor> A, std::vector<std::string> tokens)
{
    //A->State = EXIT;
    
    
    return 0;
}

int cmd_who (std::shared_ptr<Actor> A, std::vector<std::string> tokens)
{
    if(mud)
    {
        if(A->client)
        {
            if(mud->playerProc->playerList.size() > 0)
            {
                A->client->Send("Who's online\n\r" );
                A->client->Send("-------------------------------------------------------------------------------\n\r");
                
                int i = 0;
                for(auto P : mud->playerProc->playerList)
                {
                    A->client->Send("  {:<18}",P->Name);
                    
                    i++;
                    if (i > A->client->Options.window_width / 20)
                    {
                        A->client->Send("\n\r");
                        i = 0;
                    }
                }
                
                A->client->Send("-------------------------------------------------------------------------------\n\r");
                A->client->Send("{} Players online\n\r", mud->playerProc->playerList.size());
            }
        }
    }
    return 0;
}

int cmd_tell(std::shared_ptr<Actor> A, std::vector<std::string> tokens)
{
    if(mud)
    {
        if(A->client)
        {
            if(tokens.size() > 2)
            {
                for ( auto R : mud->playerProc->playerList)
                {
                    if(R->Name.find(tokens[1]) == 0 )
                    {
                        std::string message;
                        for(int i = 2; i < tokens.size(); i++)
                        {
                            message += fmt::format("{} ",tokens[i]);
                        }
                        
                        if(message.size() > 0)
                        {
                            message.pop_back();
                        }
                        
                        A->client->Send("you tell {} '{}'\n\r",R->Name, message);
                        A->client->Send("{} tells you '{}'\n\r",A->Name, message);
                        
                        break;
                    }
                }
            }
        }
    }
    return 0;
}



