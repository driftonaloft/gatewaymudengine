
#include <memory>
#include <string>
#include <iostream>
#include <vector>
#include <memory>

#include "command.h"

#include "actor.h"
#include "system.command.h"

extern std::shared_ptr<Mud> mud;

int cmd_shutdown(std::shared_ptr<Actor> A, std::vector<std::string> tokens)
{
	//TODO: impliment rest of command this is just for test
	if(A->client)
	{
        A->client->Send("----------------------------------------------------------------------\n\r");
		A->client->Send("\n\rShutting Down Server\n\r");//network->sendall();
        A->client->Send("----------------------------------------------------------------------\n\r");
	}
	//log->write("shutting down");
    if(mud)
    {
        mud->halt();
    }
    else
    {
        std::cout<<"bad mud reference" << std::endl;
    }
	return 0;
}

int cmd_status(std::shared_ptr<Actor> A, std::vector<std::string> tokens)
{
    if(A)
    {
        if(A->client)
        {
            A->client->Send("GateWay Mud Engine ver: {} , \n\r", mud->version);
            A->client->Send("----------------------------------------------------------------------\n\r");
            A->client->Send("Client Connections :{:8} Player Connections :{:8}\n\r",(int)mud->server->clientList.size(),(int)mud->playerProc->playerList.size());
            A->client->Send("Uptime:               Frametime:            Running Since :           \n\r");
            A->client->Send("Areas:        Rooms:        Active NPCs:          Active Quest:       \n\r");
            A->client->Send("----------------------------------------------------------------------\n\r");
        }
    }
    return 0;
}
