//
//  world.h
//  Mud11
//
//  Created by drifton on 7/9/15.
//  Copyright (c) 2015 FracturedWire. All rights reserved.
//

#ifndef WORLD_H
#define WORLD_H

#include "mudobject.h"

#include <vector>


class Actor;

class MudLocation
{
public:
    MudLocation();
    ~MudLocation();
    
    std::string AreaID;
    std::string RoomID;
    
    void Save(pugi::xml_node);
    void Load(pugi::xml_node);
    
    //bool instance;
    //std::string instanceID;
    //std::string baseAreaID;
    
    //bool isWilderness;
    //int wX, wY, wZ;
};

class MudExit : public MudObject
{
public:
    MudExit();
    virtual ~MudExit();
    
    bool isDoor;
    bool isLocked;
    bool isOpen;
    
    MudLocation Destination;
    std::shared_ptr<MudExit> ReturnExit;//for control of doors
    
    void Save(pugi::xml_node node);
    void Load(pugi::xml_node node);
};

class Room : public MudObject
{
public:
    Room();
    ~Room();
    
    std::vector<std::shared_ptr<Actor>> actors;
    std::vector<MudObject> items;//decide if actors should remain sperate or be commbined into this list
    std::vector<MudExit> exits;
    
    //handelevents -- map of scripts for events
    
    virtual void Save(pugi::xml_node);
    virtual void Load(pugi::xml_node);
};

class Area : public MudObject
{
public:
    std::vector<std::shared_ptr<Room>> roomList;
    
    std::string createRoom(std::string RoomID);
    void remRoom(std::string RoomID);
    std::shared_ptr<Room> getRoom(std::string RoomID);
    
    void SaveXML(std::string xmlFile);
    void LoadXML(std::string xmlFile);
};


class World : public MudObject
{
public:
    std::vector<std::shared_ptr<Area>> areaList;
    
    std::shared_ptr<Room> getRoom(std::string AreaID, std::string RoomID);
    std::shared_ptr<Room> getRoom(MudLocation Location);
    
    std::shared_ptr<Area> getArea(std::string AreaID);
    std::string createArea(std::string AreaID);
    void removeArea(std::string AreaID);
    
    void SaveXML(std::string xmlFile);
    void LoadXML(std::string xmlFile);
    
    //wildernessmap goes here
    
};

#endif /* defined(__Mud11__world__) */
