//main.cpp
//gateway mud engine
//

#include <iostream>
#include <string>
#include <memory>

#include "mud.h"
#include "accounts.h"
#include "player.h"

using namespace std;

shared_ptr<Mud>  mud;

int main(int argc, char const* argv[])
{
    cout << "Gateway mud version 0.01.0" << endl;

    mud = make_shared<Mud>();
    mud->server = std::make_shared<Server>();
    mud->accountDB = std::make_shared<AccountDB>();
    mud->playerProc = std::make_shared<PlayerProcessor>();

    if(mud->startup("config.xml"))
    {
        mud->run();
    }
    mud->shutdown();

    return 0;
}

