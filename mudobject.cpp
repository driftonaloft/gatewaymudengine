//mudobject.cpp

#include "mudobject.h"

#include "pugixml.hpp"

MudObject::MudObject()
{
    Name = "default";
    Type = "MudObject";
}

MudObject::~MudObject()
{
}

void MudObject::Save(pugi::xml_node node)
{
    node.append_child("Name").child_value(Name.c_str());
    node.append_child("Type").child_value(Type.c_str());
    
    auto propertyMapNode = node.append_child("PropertyMap");
    for(auto it = PropertyMap.begin(); it != PropertyMap.end(); it++)
    {
        auto propertyNode = propertyMapNode.append_child("Property");
        propertyNode.append_attribute("Name").set_value((it->first).c_str());
        propertyNode.set_value((it->second).c_str());
    }
}

void MudObject::Load(pugi::xml_node node)
{
    Name = node.child_value("Name");
    Type = node.child_value("Type");
    
    auto propertyMapNode = node.child("PropertyMap");
    for(pugi::xml_node property : propertyMapNode.children("Property"))
    {
        std::string propertyName = property.attribute("Name").value();
        PropertyMap[propertyName]= property.child_value();
    }
}


