//network.h
//
#ifndef NETWORK_H
#define NETWORK_H

#include <memory>
#include <string>
#include <list>
#include <vector>

#include <chrono>

#ifdef WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>

    typedef int socklen_t;
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <sys/select.h>
    #include <netdb.h>
    #include <arpa/inet.h>
    #include <netinet/in.h>
#endif

#include "mud.h"
#include "accounts.h"
#include "format.h"

enum TELNET {

	NULL_CHAR = 0,
	IAC = 255,
//negotiation commands
	DONT = 254,
	DO = 253,
	WONT = 252,
	WILL = 251,

//single commands
	SB =  250,
	GA =  249,
	EL =  248,
	EC =  247,
	AYT = 246,
	AO =  245,
	IP =  244,
	BRK = 243,
	DM =  242,
	NOP = 241,
	SE =  240,
//telnet commands
	ECHO = 1,
};

enum CLIENTSTATE {
	DEAD,
	NEW,
	NEW_CONNECTION,
	CONNECTED,
	ACCOUNT,
	ACCOUNT_PASSWORD,
	ACCOUNT_NEW,
	ACCOUNT_NEW_PASSWORD
};

class ClientOptions
{
public:
	bool telnet_echo;
	//other telnet options go here
	int window_width;
	//int telnet_height;
	//bool mssp
	//bool mccp
	//bool mxp
    bool ansi_color;
	//bool expanded_color // 256/true color
};


class Client
{
public:
    Client();
    ~Client();

	template <typename... Args>
    void Send(const char * formatString, const Args&... args)
    {
        outBuffer += fmt::format(formatString, args...);
    };
    
    std::string ansi_color(std::string input, bool enabled=true);

    long Read();
    long Write();
    void Close();
    void Process();

    unsigned long Line();
    std::string GetLine();

	bool isDead;

    std::string lineBuffer;
    std::vector<std::string> lineList;


    std::string inBuffer;
    std::string outBuffer;

    //std::shared_ptr<Actor> character;
    std::shared_ptr<Account> account;

    CLIENTSTATE clientState;
    ClientOptions Options;

    int socketFD;
};

class Mud;

class Server
{
public:
    Server();
    ~Server();

    bool Init(int Port, int MaxClients);
    bool Frame();
    bool Shutdown();

	int numberOfClients;
	int maxNumberOfClients;

	unsigned int serverSocket;
	int port;
	int MaxFD;

	fd_set master;

    std::list<std::shared_ptr<Client> > clientList;
};

#endif //NETWORK_H
