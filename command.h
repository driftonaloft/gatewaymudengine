//command.h
#include "actor.h"
#include "system.command.h"
#include "player.command.h"

#ifndef COMMAND_H
#define COMMAND_H

#include <functional>
#include <memory>
#include <string>
#include <vector>

class Command
{
public:
	Command();
	~Command();
	
	std::function <int(std::shared_ptr<Actor>,std::vector<std::string>)> fp;
	std::string cmdName;
	std::string usage;
	
	bool isExact;
	//std::string permissions
	
};

class CommandParser
{
public:
	CommandParser();
	~CommandParser();
	
	void RegisterCommand(std::string, std::function <int(std::shared_ptr<Actor>, std::vector<std::string>)>, std::string);
	std::vector<std::string> Tokenize(std::string Input);
	int Parse(std::shared_ptr<Actor>, std::string);
	
	std::vector<std::shared_ptr<Command>> Commands;
};

#endif
