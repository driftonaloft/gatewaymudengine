// mudobject.h

#ifndef MUDOBJECT_H
#define MUDOBJECT_H

#include <string>
#include <map>

#include "pugixml.hpp"


class MudObject
{
public:
    MudObject();
    virtual ~MudObject();

    virtual void Save(pugi::xml_node node);
    virtual void Load(pugi::xml_node node);

    std::string Name;
    std::string Type;

    std::map<std::string, std::string> PropertyMap;
};


#endif //MUDOBJECT_H
