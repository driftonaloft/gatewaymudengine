//accounts.h

#ifndef ACCOUNTS_H
#define ACCOUNTS_H

#include <memory>
#include <string>
#include <list>

class Account
{
public:
	Account();
	~Account();

	//std::string toEncodedPassword(std::string); // md5s the inputed password

	std::string name;
	std::string file;
	std::string password;

	bool resetPassword;
	bool newAccount;
	bool isSaveable;
	//load;
	//save;
};

class AccountDB
{
public:
	AccountDB();
	~AccountDB();

	bool init(std::string AccountDBFile);
	bool frame();
	bool halt();

    bool validID(std::string accountName);

	bool loadDB(std::string File);
	bool saveDB(std::string File);

	std::shared_ptr<Account> getAccount(std::string AccountName); // return new account if account is not located in database
	bool addAccount(std::shared_ptr<Account> account);

	std::list <std::shared_ptr<Account>> accountList;
	std::string accountFile;
};

#endif
