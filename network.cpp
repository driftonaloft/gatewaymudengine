//network.cpp
//
//

#include <iostream>
#include <memory>

#include "network.h"
#include "format.h"
#include "mud.h"

extern std::shared_ptr<Mud> mud;

Client::Client()
{
	isDead = false;
}

Client::~Client()
{
}



long Client::Read()
{
	long bytes = 0;
	char buffer[1024];

	bytes = recv(socketFD, buffer, 1024,0);
	inBuffer.append(buffer,bytes);

	return bytes;
}

long Client::Write()
{
	long bytes = outBuffer.size();
	long bytesSent  = 0;
	long totalBytesSent = 0;

    outBuffer = ansi_color(outBuffer);

	while(bytes > 0)
	{
		bytesSent = send(socketFD, outBuffer.c_str(),bytes,0);
		if(bytesSent > 0){
			outBuffer = outBuffer.erase(0,bytesSent);
			totalBytesSent += bytesSent;
			bytes = outBuffer.size();
		} else {
			std::cout << "[NETWORK] Error Send Attempt" << std::endl;
            break;
        }
	}

	return totalBytesSent;
}

void Client::Close()
{
	Write();
	shutdown(socketFD,2);
	isDead = true;
}

unsigned long Client::Line()
{
	return lineList.size();
}
std::string Client::GetLine()
{
	std::string line;
	if(lineList.size() > 0)
	{
		line = lineList.front();
		lineList.erase(lineList.begin());
	}
	return line;
}

void Client::Process()
{
    if(inBuffer.size() > 0)
    {
        for (auto c  :  inBuffer)
        {
            switch (c)
            {
                case '\b':
                    if(lineBuffer.size() > 0){
                        lineBuffer.pop_back();
                    }
                    break;
                case '\n':
                	if(lineBuffer.size() >  0)
                    {
						lineList.push_back(lineBuffer);
                    	lineBuffer.clear();
					}
                    break;
                case '\r':
                    break;
                default:
                    lineBuffer += c;
            }
        }
        std::cout << lineBuffer << std::endl;
        inBuffer.clear();
    }
}

std::string Client::ansi_color(std::string input, bool enabled)
{
    std::string output;

    for(auto c  = input.begin(); c != input.end(); c++)
    {
        if(*c == '&')
        {
            c++;
            switch (*c)
            {
                case '&':
                    output += '&';
                    break;
                case 'x':
                case 'X':
                    output += "\033[0;37m";
                    break;
                case 'N':
                case 'n':
                    output += "\n\r";
                    break;
                case '*':
                    output += "\033[8m";
                    break;
                case 'r': //dark red
                    output += "\033[0;31m";
                    break;
                case 'R': //bright red
                    output += "\033[1;31m";
                    break;
                case 'g': //green
                    output += "\033[0;32m";
                    break;
                case 'G': // bright green
                    output += "\033[1;32m";
                    break;
                case 'y': // yellow
                    output += "\033[0;33m";
                    break;
                case 'Y': // bright yellow
                    output += "\033[1;33m";
                    break;
                case 'b': // blue
                    output += "\033[0;34m";
                    break;
                case 'B': // bright blue
                    output += "\033[1;34m";
                    break;
                case 'm': // megenta
                    output += "\033[0;35m";
                    break;
                case 'M': //bright megenta
                    output += "\033[1;35m";
                    break;
                case 'c': // cyan
                    output += "\033[0;36m";
                    break;
                case 'C': // bright cyan
                    output += "\033[1;36m";
                    break;
                case 'w': // white
                    output += "\033[0;37m";
                    break;
                case 'W': // bright white
                    output += "\033[1;37m";
                    break;
                case 'p': //
                    output += "\033[40;30m";
                    break;
                default:
                    output += *c;
                    break;

            }
        } else {
            output += *c;
        }
    }

    return output;
}

Server::Server()
{
}

Server::~Server()
{

}


bool Server::Init(int Port, int MaxConnections)
{
	std::cout << "[NETWORK]Starting Server...." << std::endl;
	maxNumberOfClients = MaxConnections;
	port = Port;

#ifdef WIN32
	WORD wVer;
	WSADATA wsaData;
	wVer = MAKEWORD(2,2);
	WSAStartup(wVer, &wsaData);
#endif

	addrinfo hints, *res, *p;
	memset(&hints, 0,sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	getaddrinfo(0, "5891", &hints, &res);


	int yes = 1;

	for (p = res; p!= NULL; p = p->ai_next)
	{
		serverSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
#ifdef WIN32
		if(serverSocket == INVALID_SOCKET){
#else
		if(serverSocket == -1){
#endif
			std::cout << "[NETWORK]Socket Creation Failed" << std::endl;
			continue;
		}

		setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes,sizeof(int));
#ifdef WIN32
		if(bind(serverSocket, p->ai_addr, p->ai_addrlen) == SOCKET_ERROR){
#else
		if(bind(serverSocket, p->ai_addr, p->ai_addrlen) < 0){
#endif
			shutdown (serverSocket,2);
			continue;
		}

		break;
	}

	if(p == NULL)
	{
		std::cout << "[NETWORK] Failed to Bind Socket" << std::endl;
		return false;
	}

	freeaddrinfo(res);

#ifdef WIN32
	if( listen(serverSocket, 10) == SOCKET_ERROR){
#else
	if( listen(serverSocket, 10) == -1){
#endif
		std::cout << "[NETWORK] Failed to Open Server Socket for Listening" << std::endl;
		return false;
	}

	MaxFD = serverSocket;
	FD_ZERO(&master);
	FD_SET(serverSocket, &master);


	std::cout << "[NETWORK] Server Initialization Compleate" << std::endl;
	return true;
}

bool Server::Frame()
{
	std::shared_ptr<Client> newClient;
	timeval tv;
	sockaddr_storage clientAddr;
	socklen_t sockaddr_size = sizeof(sockaddr_in6);

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	fd_set read;
	FD_ZERO(&read);

	read = master;

	select(MaxFD+1, &read, 0, 0, &tv);

	if(FD_ISSET(serverSocket, &read))
	{
		//New Connection
		int clientSocket = accept(serverSocket, (struct sockaddr*)&clientAddr,&sockaddr_size);
#ifdef WIN32
        if(clientSocket == INVALID_SOCKET){
#else
        if(clientSocket == -1){
#endif
			std::cout << "[NETWORK] ERROR ACCEPT" << std::endl;
		} else {
			std::cout << "[NETWORK] NEW CONNECTION" << std::endl;
			FD_SET(clientSocket, &master);
			newClient = std::make_shared<Client>();
			newClient->socketFD = clientSocket;
			newClient->clientState = NEW;
            newClient->Send("[Connected]\n\r");
			clientList.push_back(newClient);
			if(clientSocket > MaxFD) {
				MaxFD = clientSocket;
			}
		}
	}

	for(auto c  : clientList)
	{
		c->Write();
        std::string input;
		if(FD_ISSET(c->socketFD,&read))
		{
			if(c->Read() == 0)
			{
				std::cout << "[NETWORK] - CONNECTION LOST" << std::endl;
				FD_CLR(c->socketFD,&master);
				c->Process();
				c->Close();
                continue;
            }
        }
        c->Process();
        switch (c->clientState)
        {
            case NEW: //connected
                c->Send("&WW&welcome to &GG&gatewayMud&n&xPlease enjoy your stay\n\r&WUSERNAME &w>&x ");
                c->clientState = NEW_CONNECTION;
                break;
            case NEW_CONNECTION://attempt login
                if(c->Line())
                {
                    input = c->GetLine();
                    auto account = mud->accountDB->getAccount(input);
                    if(account)
                    {
                        c->Send("&xWelcome Back {} &nUSER PASSWORD:> ", account->name);
                        c->account = account;
                        c->clientState = ACCOUNT_PASSWORD;
                    }
                    else {
                    	c->Send("&xWelcome Player {}&n",input);
                    	c->Send("&xWould you like to create a new account (&WY&w/&WN&x)? ");
                    	c->account = std::make_shared<Account> ();
                    	c->account->name = input;
						c->clientState = ACCOUNT_NEW;
                    }
                    break;
                }
                break;
            case ACCOUNT_PASSWORD:
            	if(c->Line())
            	{
            		input = c->GetLine();
            		if(input == c->account->password)
            		{
            			c->Send("&x[&WConnected&x]&n:> ");

                        std::shared_ptr<Player> player = std::make_shared<Player>();
                        //player->loadFile(c->account->File);

                        player->Name = c->account->name;
                        player->client = c;

                        mud->playerProc->playerList.push_back(player);
                        c->clientState = CONNECTED;
					}
				}
				break;
			case ACCOUNT_NEW:
				if(c->Line())
				{
					input = c->GetLine();
					if(/*toUpperCase*/ input == "y" || input == "Y") {
						c->Send("Please Enter a password :> ");
						c->clientState = ACCOUNT_NEW_PASSWORD;
					} else {
                        c->clientState = NEW;
                    }
				}
				break;
			case ACCOUNT_NEW_PASSWORD:
				if(c->Line())
				{
					input = c->GetLine();
					c->account->password = input;
					c->account->file = account->name += ".xml";
					c->account->isSaveable = true;
					mud->accountDB->accountList.push_back(c->account);

                    std::shared_ptr<Player> player = std::make_shared<Player>();
                    //player->loadFile(c->account->File);

                    player->Name = c->account->name;
                    player->client = c;

                    mud->playerProc->playerList.push_back(player);
					c->Send("&x[&WWelcome&x]&n");
                    c->clientState = CONNECTED;

				}
                break;
			case CONNECTED:
				break;
			case DEAD:
					c->isDead = true;
				break;
			default:
				break;

        }
	}

	clientList.remove_if([](std::shared_ptr<Client> c){return c->isDead;});
    return true;
}

bool Server::Shutdown()
{
    for(auto c: clientList)
    {
        c->Send("&W[&RShutting Down Server&W]&x&n");
        c->Write();
        c->Close();
    }

    shutdown(serverSocket,2);

    return true;
}


