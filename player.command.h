//
//  player.command.h
//  Mud11
//
//  Created by drifton on 7/4/15.
//  Copyright (c) 2015 FracturedWire. All rights reserved.
//

#ifndef Mud11_player_command_h
#define Mud11_player_command_h

#include "actor.h"

int cmd_exit(std::shared_ptr<Actor> A, std::vector<std::string> tokens);
int cmd_who (std::shared_ptr<Actor> A, std::vector<std::string> tokens);
int cmd_tell(std::shared_ptr<Actor> A, std::vector<std::string> tokens);


#endif
