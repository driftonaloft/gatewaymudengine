//player.cpp


#include <iostream>
#include <memory> 
#include <string>

#include "format.h"
#include "mud.h"
#include "player.h"


extern std::shared_ptr<Mud> mud;

Player::Player()
{

}

Player::~Player()
{
}

void Player::Load(std::string File)
{
}

void Player::Save(std::string File)
{

}

PlayerProcessor::PlayerProcessor()
{
}

PlayerProcessor::~PlayerProcessor()
{
}

bool PlayerProcessor::Init()
{
    std::cout << "[PlayerProc]" << std::endl;
    return true;
}

bool PlayerProcessor::Frame()
{
    for(auto player : playerList)
    {
        if(player->client)
        {
            if(player->client->clientState == CONNECTED)
            {
                if(player->client->Line())
                {
	            	std::string input = player->client->GetLine();

					mud->cmdParser->Parse(std::dynamic_pointer_cast<Actor>(player),input);
                    player->client->Send("%W:> %X");
                    
                }
            }
        }
        //player->tick();
    }
    return true;
}

bool PlayerProcessor::Shutdown()
{
    for(auto player: playerList)
    {
        //player->save(player->file);
    }
    return true;
}
