//
//  world.cpp
//  Mud11
//
//  Created by drifton on 7/9/15.
//  Copyright (c) 2015 FracturedWire. All rights reserved.
//

#include "pugixml.hpp"

#include "world.h"
#include "network.h"
#include "actor.h"

//MudLocation
MudLocation::MudLocation()
{
    AreaID = "default";
    RoomID = "default";
}

MudLocation::~MudLocation()
{
}

void MudLocation::Load(pugi::xml_node node)
{
    AreaID = node.child_value("AreaID");
    RoomID = node.child_value("RoomID");
}

void MudLocation::Save(pugi::xml_node node)
{
    node.append_child("AreaID").child_value(AreaID.c_str());
    node.append_child("RoomID").child_value(RoomID.c_str());
}

//MudExit

MudExit::MudExit() : MudObject()
{
    isDoor = false;
    isLocked = false;
    isOpen = true;
}

MudExit::~MudExit()
{
}

void MudExit::Save(pugi::xml_node node)
{
    if(node)
    {
        
        MudObject::Save(node);
        
        node.append_child("isDoor").text() = isDoor;
        node.append_child("isLocked").text() = isLocked;
        node.append_child("isOpen").text() = isOpen;
        
        Destination.Save(node.append_child("Destination"));
    }
}

void MudExit::Load(pugi::xml_node node)
{
    if(node)
    {
        
        MudObject::Load(node);
        
        isDoor = node.child("isDoor").text().as_bool();
        isLocked = node.child("isLocked").text().as_bool();
        isOpen = node.child("isOpen").text().as_bool();
        
        Destination.Load(node.child("Destination"));
        //loading code goes here
    }
}

//Room

Room::Room()
{
    Type = "room";
    Name = "Default Room Object Name";
    PropertyMap["description"] = "Default Description, Please Change Me{n";
}

Room::~Room()
{
}

void Room::Load(pugi::xml_node node)
{
    if(node)
    {
        MudObject::Load(node);
        
        
        auto ActorsNode = node.child("actors");
        if(ActorsNode)
        {
            for (auto childNode : ActorsNode.children("actor"))
            {
                std::shared_ptr<Actor> A = std::make_shared<Actor> (LoadActor(childNode.child_value("type")));
            }
        }
         
        /*
        auto ItemNode = node.child("items");
        if(ItemNode)
        {
        for (auto childNode : ItemNode.children("item"))
        {
            //if statement to load speciffic types // might want to roll into its own function to be called at other locations like player load and within items takes a child node and returns default Item
        }
        }
        */
        
        auto ExitNode = node.child("exits");
        if(ExitNode)
        {
            for (auto childNode : ExitNode.children("exit"))
            {
                MudExit Exit;
                Exit.Load(childNode);
                exits.push_back(Exit);
            }
        }
    }
}

void Room::Save(pugi::xml_node node)
{
    if(node)
    {
        MudObject::Save(node);
        
        if(actors.size() > 0)
        {
            auto actorNode = node.append_child("actors");
            for(auto A : actors)
            {
                
                if(A->Type != "player")
                {
                    A->Save(actorNode.append_child("actor"));
                }
                else
                {
                    std::shared_ptr<Player> P = std::make_shared<Player> (A);
                    P->Save(P->File);
                }
            }
        }
        
        if(exits.size() > 0)
        {
            auto exitNode = node.append_child("exits");
            for (auto E: exits)
            {
                
                E.Save(exitNode.append_child("exit"));
            }
        }
    }
}





